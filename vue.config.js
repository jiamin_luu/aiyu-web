const { defineConfig } = require('@vue/cli-service')
const NodePolyfillPlugin = require('node-polyfill-webpack-plugin')
module.exports = defineConfig({
    transpileDependencies: true,
    publicPath: './',
    configureWebpack: {
        plugins: [new NodePolyfillPlugin()]
    },
    devServer: {
        client: {
            progress: true,
            overlay: false
        },
        proxy: {
            '/baseApi': {
                target: 'http://10.1.79.85:7861',
                changeOrigin: true,
                pathRewrite: { '^/baseApi': '' }
            },
            '/knowledge': {
                target: 'http://10.1.79.85:8080',
                // target: 'http://10.1.79.102:8080',
                changeOrigin: true,
                pathRewrite: { '^/knowledge': '' }
            },
            '/mapIframe': {
                target: 'http://10.1.7.147:8080/',
                changeOrigin: true,
                pathRewrite: { '^/mapIframe': '' }
            },
        }
    }
})