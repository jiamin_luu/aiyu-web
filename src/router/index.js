import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/home/index.vue'

Vue.use(VueRouter)

const routes = [{
        path: '/',
        name: 'home',
        component: HomeView,
    },
    {
        path: '/login',
        name: 'login',
        component: () =>
            import ('@/views/login/index'),
    },
];


const router = new VueRouter({
    // mode: 'hash',
    // base: process.env.BASE_URL,
    routes
})

router.beforeEach(async(to, from, next) => {
    let token = localStorage.getItem('token')
    let expirse = Number(localStorage.getItem('expirse'));
    let tm = new Date().getTime();
    if (!token && to.name !== 'login') {
        next({ name: 'login' })
    } else if (to.name !== 'login' && tm > expirse) {
        localStorage.clear();
        next({ name: 'login' })
    } else {
        next()
    }
})
export default router