import pageInation from "@/components/pageInation/index.vue";
import parentDialog from "@/components/dialog/parentDialog.vue"; // 弹窗
import prompt from "@/views/fileManagement/components/tools/dialog/prompt.vue";
import shot from "@/views/fileManagement/components/tools/dialog/shot.vue";
import {} from "@/api/tools.js";
import { downLoad, delay } from "@/utils/index";
export const mixins = {
    components: { pageInation, parentDialog, prompt, shot },
    data() {
        return {
            dialogVisible: false,
            dialogTitle: "",
            dialogHeight: "25%",
            dialogWidth: "40%",
        };
    },
    computed: {},
    created() {},
    mounted() {},
    methods: {
        handlePrompt(row) {
            this.dialogHeight = "55%";
            this.dialogWidth = '40%'
            this.toolInfo = row;
            this.dialogTitle = `${row.resourceName || row.tkName} Prompt修改`;
            this.dialogVisible = true;
        },
        handelShots(row) {
            this.dialogHeight = "55%";
            this.dialogWidth = '60%'
            this.toolInfo = row;
            // this.dialogTitle = "shot";
            this.dialogTitle = `${row.resourceName || row.tkName} shot修改`;
            this.dialogVisible = true;
        },
    },
};