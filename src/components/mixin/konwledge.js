import store from "@/store/index";
import { Message } from "element-ui";
import txt from "@/components/priviewFile/txt.vue";
import pdf from "@/components/priviewFile/pdf.vue";
import docx from "@/components/priviewFile/docx.vue";
import xlsx from "@/components/priviewFile/xlsx.vue";
import parentDialog from "@/components/dialog/parentDialog.vue"; // 弹窗
import upload from "@/components/dialog/upload.vue"; // 文件上传
import pageInation from "@/components/pageInation/index.vue";
import filedDetails from "@/components/dialog/filedDetails.vue";
import { createTempKnowledge, deleteKnowledge, deleteDocsJava } from "@/api/KnowledgeBaseManagement";
import { downLoad, delay } from "@/utils/index";
export const mixins = {
    components: { pageInation, parentDialog, upload, txt, pdf, docx, xlsx, filedDetails },
    data() {
        return {
            image: {
                txt: require('@/assets/image/icon/txt.svg'),
                pdf: require('@/assets/image/icon/pdf.svg'),
                docx: require('@/assets/image/icon/docx.svg')
            },
            userName: localStorage.getItem('userName'),
        };
    },
    computed: {
        userRole() {
            return localStorage.getItem("roles") == "超级管理员";
        },
    },
    created() {},
    mounted() {},
    methods: {
        // 批量提问
        async batchQuestioning() {
            let selection = this.$refs.multipleTable.selection;
            if (selection.length) {
                store.commit('toggle_overallLoading', true);
                this.$Bus.$emit("batchQuestioning");
                // this.$Bus.$emit("batchQuestioning", `dev-cx-temp-${this.userName}`);
                let fileNames = selection.map((v) => v.fileName);
                let res = await deleteKnowledge({ knowledgeBaseName: `dev-cx-temp-${this.userName}` });
                createTempKnowledge({
                        sourceKbId: this.userknowId,
                        tempKnowledgeBaseName: `dev-cx-temp-${this.userName}`,
                        fileNames: fileNames,
                    })
                    .then((result) => {
                        console.log(result, "resultresultresultresultresultresult");
                        if (result.code == 200) {
                            Message.success(result.msg)
                            this.$Bus.$emit("batchQuestioning", result.data.kbId);
                        } else {
                            Message.error(result.msg)
                        }
                        store.commit('toggle_overallLoading', false)
                    })
                    .catch((err) => {
                        console.log(err);
                        store.commit('toggle_overallLoading', false)
                    });
            } else {
                this.$message.warning("请选择文件后再批量提问");
            }
        },
        // 下载
        async operate(row) {
            if (row) {
                downLoad(row.fileName, row.url);
            } else {
                console.log(this.$refs.multipleTable.selection);
                let selection = this.$refs.multipleTable.selection || [];
                if (!selection.length) {
                    this.$message.warning("请先选择文件");
                    return;
                }
                for (let i = 0; i < selection.length; i++) {
                    await delay(100);
                    const item = selection[i];
                    downLoad(item.fileName, item.url);
                }
            }
        },
        // 删除
        deleteFile(row) {
            let fileNameList = [],
                fileIdList = [];
            if (row["id"]) {
                fileNameList = [row.fileName];
                fileIdList = [row.id];
            } else {
                let selection = this.$refs.multipleTable.selection;
                if (selection.length) {
                    fileNameList = selection.map((v) => v.fileName);
                    fileIdList = selection.map((v) => v.id);
                }
            }

            if (!fileIdList.length) {
                this.$message.warning("请先选择文件");
                return;
            }
            this.$confirm("确定删除文件?", "提示", {
                cancelButtonText: "取消",
                confirmButtonText: "确定",
                type: "warning",
            }).then(() => {
                deleteDocsJava({ ids: fileIdList })
                    .then((result) => {
                        if (result.code == "200") {
                            this.$message.success(result.msg);
                            this.getTableList();
                        }
                    })
                    .catch((err) => {
                        console.log(err);
                    });
            });
        },
    },
};