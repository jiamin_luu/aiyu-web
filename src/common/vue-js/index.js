import Vue from 'vue'
import Storage from 'vue-ls'
const storageOptions = {
  namespace: 'mirco_child__', // key prefix
  name: 'ls', // name variable Vue.[ls] or this.[$ls],
  storage: 'local' // storage name session, local, memory
}

Vue.use(Storage, storageOptions)
// export default (app) => {
//     if (window.Vue) {
//         window.Vue.use(Storage, storageOptions);
//     } else {
//         app.use(Storage, storageOptions);
//     }

// }
