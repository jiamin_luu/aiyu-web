/**
 * @Description:海康视频
 * @author xxx
 * @date 2023/04/24
 */
import { Message } from 'element-ui'
// import './web-control_1.2.5.min'
export default class VideoIsc {
  /**
   *  @param  cameraIndexCode 监控点编号
   */
  constructor(cameraIndexCode) {
    //公共变量
    this.cameraIndexCode = cameraIndexCode
    this.initCount = 0
    this.pubKey = ''
    this.oWebControl = null
    this.initPlugin()
    window.addEventListener(
      'resize',
      () => {
        this.resize()
      },
      false
    )
  }

  // 创建播放实例
  initPlugin() {
    var _this = this
    this.oWebControl = new WebControl({
      szPluginContainer: 'playWnd', // 指定容器id
      iServicePortStart: 15900, // 指定起止端口号，建议使用该值
      iServicePortEnd: 15900,
      szClassId: '23BF3B0A-2C56-4D97-9C03-0CB103AA8F11', // 用于IE10使用ActiveX的clsid
      cbConnectSuccess: function () {
        // console.log(_this.oWebControl)
        // 创建WebControl实例成功

        // _this.oWebControl.JS_HideWnd()
        _this.oWebControl
          .JS_StartService('window', {
            // WebControl实例创建成功后需要启动服务
            dllPath: './VideoPluginConnect.dll' // 值"./VideoPluginConnect.dll"写死
          })
          .then(
            function () {
              // 启动插件服务成功
              _this.oWebControl.JS_SetWindowControlCallback({
                // 设置消息回调
                cbIntegrationCallBack: _this.cbIntegrationCallBack
              })

              _this.oWebControl?.JS_CreateWnd('playWnd', 1000, 600).then(function () {
                //JS_CreateWnd创建视频播放窗口，宽高可设定
                _this.init() // 创建播放实例成功后初始化
              })
            },
            function () {
              // 启动插件服务失败
            }
          )
      },
      cbConnectError: function () {
        // 创建WebControl实例失败
        _this.oWebControl = null
        Message.warning('插件未启动，正在尝试启动，请稍候...')
        // alert('插件未启动，正在尝试启动，请稍候...')
        console.log('插件未启动，正在尝试启动，请稍候...');
        WebControl.JS_WakeUp('VideoWebPlugin://') // 程序未启动时执行error函数，采用wakeup来启动程序
        _this.initCount++
        if (_this.initCount < 3) {
          setTimeout(function () {
            initPlugin()
          }, 3000)
        } else {
          alert('插件启动失败，请检查插件是否安装！')
          // Message.warning('插件启动失败，请检查插件是否安装！')
        }
      },
      cbConnectClose: function (bNormalClose) {
        // 异常断开：bNormalClose = false
        // JS_Disconnect正常断开：bNormalClose = true
        _this.oWebControl = null
        Message.warning('插件未启动，正在尝试启动，请稍候...')
        // alert('插件未启动，正在尝试启动，请稍候...');
        console.log('插件未启动，正在尝试启动，请稍候...');
        WebControl.JS_WakeUp('VideoWebPlugin://')
        _this.initCount++
        if (_this.initCount < 3) {
          setTimeout(function () {
            initPlugin()
          }, 3000)
        } else {
          Message.warning('插件启动失败，请检查插件是否安装！')
          // alert('插件启动失败，请检查插件是否安装！')
        }
      }
    })
  }

  // 设置窗口控制回调
  setCallbacks() {
    this.oWebControl.JS_SetWindowControlCallback({
      cbIntegrationCallBack: this.cbIntegrationCallBack
    })
  }

  // 推送消息
  cbIntegrationCallBack(oData) {
    return
    // showCBInfo(JSON.stringify(oData.responseMsg));
  }

  //初始化
  init() {
    var _this = this
    this.getPubKey(function () {
      ////////////////////////////////// 请自行修改以下变量值	////////////////////////////////////
      var appkey = '21430310' //综合安防管理平台提供的appkey，必填
      var secret =  "7CTdNXg09greiwGy5S7c";  //综合安防管理平台提供的secret，必填
      var ip = '222.189.187.203' //综合安防管理平台IP地址，必填
      var port =  11443 //综合安防管理平台端口，若启用HTTPS协议，默认443
      var playMode = 0 //初始播放模式：0-预览，1-回放
      var snapDir = 'D:\\SnapDir' //抓图存储路径
      var videoDir = 'D:\\VideoDir' //紧急录像或录像剪辑存储路径
      var layout = '1x1' //playMode指定模式的布局
      var enableHTTPS = 1 //是否启用HTTPS协议与综合安防管理平台交互，这里总是填1
      var encryptedFields = '' //加密字段，默认加密领域为secret
      var showToolbar = 0 //是否显示工具栏，0-不显示，非0-显示
      var toolBarButtonIDs = '4097,4098' //工具栏按钮字符串，多个之间以“,”分割
      var showSmart = 0 //是否显示智能信息（如配置移动侦测后画面上的线框），0-不显示，非0-显示
      var buttonIDs = '0,512' //自定义工具条按钮
      // var buttonIDs = '0,16,256,257,258,259,260,512,513,514,515,516,517,768,769' //自定义工具条按钮
      ////////////////////////////////// 请自行修改以上变量值	////////////////////////////////////
      console.log(window.location.host.split(':')[0], ip)

      _this.oWebControl
        .JS_RequestInterface({
          funcName: 'init',
          argument: JSON.stringify({
            appkey: appkey, //API网关提供的appkey
            secret: secret, //API网关提供的secret
            ip: ip, //API网关IP地址
            playMode: playMode, //播放模式（决定显示预览还是回放界面）
            port: port, //端口
            snapDir: snapDir, //抓图存储路径
            videoDir: videoDir, //紧急录像或录像剪辑存储路径
            layout: layout, //布局
            enableHTTPS: enableHTTPS, //是否启用HTTPS协议
            encryptedFields: encryptedFields, //加密字段
            showToolbar: showToolbar, //是否显示工具栏
            showSmart: showSmart, //是否显示智能信息
            buttonIDs: buttonIDs, //自定义工具条按钮
            toolBarButtonIDs
          })
        })
        .then(function (oData) {
          _this.startPreview(_this.cameraIndexCode, -1)
          _this.resize()
          // _this.oWebControl.JS_Resize(1000, 600) // 初始化后resize一次，规避firefox下首次显示窗口后插件窗口未与DIV窗口重合问题
        })
    })
  }

  //获取公钥
  getPubKey(callback) {
    var _this = this
    this.oWebControl
      .JS_RequestInterface({
        funcName: 'getRSAPubKey',
        argument: JSON.stringify({
          keyLength: 1024
        })
      })
      .then(function (oData) {
        // console.log(oData)
        if (oData.responseMsg.data) {
          _this.pubKey = oData.responseMsg.data
          callback()
        }
      })
  }

  //RSA加密
  setEncrypt(value) {
    var encrypt = new JSEncrypt()
    encrypt.setPublicKey(this.pubKey)
    return encrypt.encrypt(value)
  }

  /**
   * 开始播放
   * @param {*} cameraIndexCode 监控点编号(必填)
   */
  startPreview(cameraIndexCode, idx = 1) {
    var streamMode = 0 //主子码流标识：0-主码流，1-子码流
    var transMode = 1 //传输协议：0-UDP，1-TCP
    var gpuMode = 0 //是否启用GPU硬解，0-不启用，1-启用
    var wndId = idx //播放窗口序号（在2x2以上布局下可指定播放窗口）

    cameraIndexCode = cameraIndexCode.replace(/(^\s*)/g, '')
    cameraIndexCode = cameraIndexCode.replace(/(\s*$)/g, '')
    // console.log(this.oWebControl, 'this.oWebControl')
    this.oWebControl.JS_RequestInterface({
      funcName: 'startPreview',
      argument: JSON.stringify({
        cameraIndexCode: cameraIndexCode, //监控点编号
        streamMode: streamMode, //主子码流标识
        transMode: transMode, //传输协议
        gpuMode: gpuMode, //是否开启GPU硬解
        wndId: wndId //可指定播放窗口
      })
    })
  }
  setLayout(layout) {
    this.oWebControl.JS_RequestInterface({
      funcName: 'setLayout',
      argument: {
        layout
      }
    })
  }
  // 停止播放
  stopAllPreview() {
    this.oWebControl.JS_RequestInterface({
      funcName: 'stopAllPreview'
    })
  }

  // 清除
  clear() {
    if (this.oWebControl != null) {
      this.oWebControl.JS_HideWnd() // 先让窗口隐藏，规避可能的插件窗口滞后于浏览器消失问题
      window.removeEventListener('resize', () => {})
      // this.oWebControl.JS_Disconnect() //
      // this.oWebControl.JS_Disconnect().then(
      //   function () {
      //     // 断开与插件服务连接成功
      //   },
      //   function () {
      //     // 断开与插件服务连接失败
      //   }
      // )
    }
  }
  // 隐藏播放器
  hidewndPlay() {
    if (this.oWebControl != null) {
      this.oWebControl.JS_HideWnd() // 先让窗口隐藏，规避可能的插件窗口滞后于浏览器消失问题
    }
  }
  // 显示播放器
  showwndPlay() {
    if (this.oWebControl != null) {
      this.oWebControl.JS_ShowWnd() // 窗口显示
    }
  }
  // 重置播放器大小
  resize() {
    var h = document.getElementById('playWnd')?.clientHeight
    var w = document.getElementById('playWnd')?.clientWidth
    if (this.oWebControl != null && h && w) {
      this.oWebControl.JS_Resize(w, h)
      // setWndCover()
    }
  }
}
