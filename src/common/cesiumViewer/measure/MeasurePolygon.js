
import { helpers, area, random, bbox, tin } from '@turf/turf'

// DrawPolygon
/*
绘制面
 */
class DrawPolygon {
  constructor(arg) {
    this.viewer = arg
    this.Cesium = Cesium
    this.callback = arg.callback
    this._polygon = null //活动面
    this._polygonLast = null //最后一个面
    this._positions = [] //活动点
    this._entities_point = [] //脏数据
    this._entities_polygon = [] //脏数据
    this._polygonData = null //用户构造面 坐标数据
  }

  //返回最后活动面
  getpolygon() {
    return this._polygonLast
  }

  //返回面数据用于加载面
  getData() {
    return this._polygonData
  }

  //加载面
  loadPolygon(data) {
    var $this = this
    return this.viewer.entities.add({
      name: 'toolGraphics',
      polygon: {
        hierarchy: new $this.Cesium.PolygonHierarchy(data),
        clampToGround: true,
        show: true,
        fill: true,
        material: $this.Cesium.Color.RED.withAlpha(0.5),
        width: 3,
        outlineColor: $this.Cesium.Color.BLACK,
        outlineWidth: 1,
        outline: false
      }
    })
  }

  //最后多边形的坐标
  get3dzuobiao() {
    //console.log(this._polygonData);
    return this._polygonData
  }

  //开始绘制
  startCreate() {
    //返回绘制的图形坐标
    return new Promise((resolve, reject) => {
      var $this = this
      this.handler = new this.Cesium.ScreenSpaceEventHandler(this.viewer.scene.canvas)
      this.handler.setInputAction(function (evt) {
        //单机开始绘制
        var cartesian = $this.getCatesian3FromPX(evt.position)
        if ($this._positions.length == 0) {
          $this._positions.push(cartesian.clone())
        }
        $this.createPoint(cartesian)
        $this._positions.push(cartesian)
      }, $this.Cesium.ScreenSpaceEventType.LEFT_CLICK)
      this.handler.setInputAction(function (evt) {
        //移动时绘制面
        if ($this._positions.length < 1) return
        var cartesian = $this.getCatesian3FromPX(evt.endPosition)
        if ($this._positions.length == 3) {
          if (!$this.Cesium.defined($this._polygon)) {
            $this._polygon = $this.createPolygon()
          }
        }
        $this._positions.pop()
        $this._positions.push(cartesian)
      }, $this.Cesium.ScreenSpaceEventType.MOUSE_MOVE)


      this.handler.setInputAction(async function (evt) {
        //右击结束绘制面
        if (!$this._polygon) return
        var cartesian = $this.getCatesian3FromPX(evt.position)
        $this._positions.pop()
        $this._positions.push(cartesian)
        // 计算面积
        let aer = $this.surfaceArea($this._positions)
        $this.createPoint(cartesian, aer)
        $this._polygonData = $this._positions.concat()
        $this.viewer.entities.remove($this._positions) //移除
        $this._positions = null
        $this._positions = []
        var Polygon = $this.loadPolygon($this._polygonData)
        $this._entities_polygon.push(Polygon)
        $this._polygonLast = Polygon
        if (typeof $this.callback == 'function') {
          $this.callback(Polygon)
        }
        $this.destroy() //销毁实例
        let zuobiao = await $this.get3dzuobiao() //获取绘制多边形后的坐标
        resolve(zuobiao) //返回坐标
      }, $this.Cesium.ScreenSpaceEventType.RIGHT_CLICK)
      /*    this.handler.setInputAction(function () {
        $this.destroy();//右键销毁实例
      }, $this.Cesium.ScreenSpaceEventType.RIGHT_CLICK);*/
    })
  }

  //创建面
  createPolygon() {
    var $this = this
    var polygon = this.viewer.entities.add({
      name: 'toolGraphics',
      polygon: {
        hierarchy: new $this.Cesium.CallbackProperty(function () {
          return new $this.Cesium.PolygonHierarchy($this._positions)
        }, false),
        clampToGround: true,
        show: true,
        fill: true,
        material: $this.Cesium.Color.RED.withAlpha(0.5),
        width: 3,
        outlineColor: $this.Cesium.Color.BLACK,
        outlineWidth: 1,
        outline: false
      }
    })
    $this._entities_polygon.push(polygon)
    return polygon
  }

   /**
   * 椭球体空间面积
   * @param vertexCollection
   * @private
   */
   surfaceArea(vertexCollection) {
    let rings = []
    vertexCollection.map((vertex) => {
      //笛卡尔转弧度
      let carto_pt = Cesium.Cartographic.fromCartesian(vertex)
      //弧度转经纬度
      rings.push([Cesium.Math.toDegrees(carto_pt.longitude), Cesium.Math.toDegrees(carto_pt.latitude)])
    })
    rings.push(rings[0])
    //转成feature Polygon
    let polygon_json = helpers.polygon([rings])
    //计算面积
    let surface = area(polygon_json)
    surface = parseFloat((surface / 1000000).toFixed(3))
    return surface
  }

  //创建点
  createPoint(cartesian, area) {
    var $this = this
    var point = this.viewer.entities.add({
      name: 'toolGraphics',
      position: cartesian,
      point: {
        pixelSize: 10,
        color: $this.Cesium.Color.YELLOW
      },
      label: {
        text: area ? area + '平方千米' : '',
        fillColor: Cesium.Color.fromCssColorString('rgb(255, 255, 255)').withAlpha(1.1),
        outlineColor: Cesium.Color.BLACK,
        outlineWidth: 2,
        pixelOffset: new Cesium.Cartesian2(10, -10),
        font: '20px Microsoft YaHei',
        disableDepthTestDistance: 15000000,
        distanceDisplayCondition: new Cesium.DistanceDisplayCondition(0, 500000)
      }
    })
    $this._entities_point.push(point)
    return point
  }

  //销毁事件
  destroy() {
    if (this.handler) {
      this.handler.destroy()
      this.handler = null
    }
  }

  //清空实体对象
  clear() {
    for (var i = 0; i < this._entities_point.length; i++) {
      this.viewer.entities.remove(this._entities_point[i])
    }
    for (var i = 0; i < this._entities_polygon.length; i++) {
      this.viewer.entities.remove(this._entities_polygon[i])
    }
    this._polygon = null //活动面
    this._polygonLast = null //最后一个面
    this._positions = [] //活动点
    this._entities_point = [] //脏数据
    this._entities_polygon = [] //脏数据
    this._polygonData = null //用户构造面
  }

  getCatesian3FromPX(px) {
    var cartesian
    var ray = this.viewer.camera.getPickRay(px)
    if (!ray) return null
    cartesian = this.viewer.scene.globe.pick(ray, this.viewer.scene)
    return cartesian
  }
}

export default DrawPolygon
