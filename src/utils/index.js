import dayjs from 'dayjs'
// import * as turf from '@turf/turf'

/**
 * 初始化时间
 * @param {Number} day 天数
 * @returns []
 */
export function getInitTime(day) {
    if (day == 0) {
        return [
            dayjs('2023-07-01 08:00:00').format("YYYY-MM-DD 08:00:00"),
            dayjs('2023-07-05 08:00:00').format(`YYYY-MM-DD HH:mm:ss`)
        ];
    } else {
        return [
            dayjs().add(day, "day").format("YYYY-MM-DD 08:00:00"),
            dayjs().format(`YYYY-MM-DD HH:mm:ss`)
        ];
    }
}
/**
 * 处理y轴的最大值最小值
 * @param {Array} arrList 处理数据
 * @param {Number} maxMultiple y轴最大值
 * @param {Number} minMultiple  y轴最小值
 * @param {Number} intervalNum 间隔个数
 * @returns
 */
export function getDataInterval(arrList, maxMultiple = 1.1, minMultiple = 0.9, intervalNum = 5) {
    let list = arrList.filter(num => num !== null && num !== undefined && num !== "" && num !== "--")
        // y轴最大、最小值
    let maxY = (Math.max(...list) * maxMultiple).toFixed(1);
    let minY = (Math.min(...list) * minMultiple).toFixed(1);
    let interval = 0
    if (maxY == 0 && minY == 0) {
        maxY = 3
        interval = 1
    } else {
        interval = ((maxY - minY) / intervalNum).toFixed(1);
    }
    return { maxY, minY, interval }
}

/**
 * 保留小数
 * @param {Number} num
 * @param {int} digit
 * @returns
 */
export function saveDecimals(num, digit) {
    if (num) {
        return Number(num).toFixed(digit)
    } else {
        return '--'
    }
}



// 获取面中心点
// export function getPolygonCenter(viewer, polygon_point_arr){
//   // 保存转换后的点数组，这个格式必须按照 turf 的要求来
//   let turf_arr = [[]]
//   // 坐标转换
//   polygon_point_arr.forEach((val) => {
//     let polyObj = {}
//     // 空间坐标转世界坐标(弧度) 同 Cesium.Cartographic.fromCartesian
//     let cartographic = viewer.scene.globe.ellipsoid.cartesianToCartographic(val)
//     // 弧度转为角度（经纬度）
//     polyObj.lon = Cesium.Math.toDegrees(cartographic.longitude)
//     polyObj.lat = Cesium.Math.toDegrees(cartographic.latitude)
//     turf_arr[0].push([polyObj.lon, polyObj.lat])
//   })

//   // turf 需要将整个点闭合，所以最后一个点必须和起点重合。
//   turf_arr[0].push(turf_arr[0][0])
//   let turf_position = turf.polygon(turf_arr)
//   let turf_position_point = turf.centerOfMass(turf_position)
//   return turf_position_point.geometry.coordinates
// }

// export function addPolylineLine(data, height = 0, color = '#ffac4d') {
//   if (data.length) {
//     if (data[0].length == 2) {
//       data = data.map(v=>[...v,height])
//     }
//     window.cesiumViewer.viewer.entities.add({
//       name:'险工险段',
//       polyline: {
//         positions: Cesium.Cartesian3.fromDegreesArrayHeights(data.flat(2)),
//         width: 2,
//         material: Cesium.Color.fromCssColorString(color)
//       }
//     })
//   }
// }

/**
 * 深拷贝
 * @param {Object} source
 */
export function deepCopy(source) {
    // 如果源对象不是对象类型，则直接返回源对象
    if (typeof source != 'object') {
        return source;
    }
    // 如果源对象为null，则直接返回null
    if (source == null) {
        return source;
    }
    // 根据源对象的构造函数是否为数组来确定新对象的类型
    var newObj = source.constructor === Array ? [] : {};
    // 遍历源对象的属性
    for (var i in source) {
        // 递归调用深拷贝函数，复制源对象的每个属性的值到新对象
        newObj[i] = deepCopy(source[i]);
    }
    return newObj;
}

/**
 * a标签模拟下载
 * @param {*} fileName 文件名称
 * @param {*} path 文件路径
 */
export function downLoad(fileName, path) {
    try {
        let random = Math.random().toFixed(2) * 100
        let aDom = document.createElement('a');
        aDom.style.display = "none";
        aDom.href = path;
        aDom.setAttribute("download", fileName);
        document.body.appendChild(aDom);
        aDom.click();
        document.body.removeChild(aDom);
    } catch (error) {
        console.log(error);
    }
}
/**
 * 延迟
 * @param {*} ms 毫秒
 * @returns 
 */
export function delay(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}
/**
 * 下载Base64编码的图片
 * @param {*} base64URL base64内容
 * @param {*} fileName 文件名称
 */
export function downloadBase64Image(base64URL, fileName) {
    // 创建a标签，用于触发下载
    const a = document.createElement("a");
    // 将 a 标签的 download 属性设置为要下载的文件名
    a.download = fileName || "image";
    // 创建 Blob 对象，并获取 base64 数据的 MIME 类型
    const mimeType = base64URL.match(/:(.*?);/)[1];
    // 将 base64 数据转换为字节数组
    const byteCharacters = atob(base64URL.split(",")[1]);
    const byteNumbers = new Array(byteCharacters.length);
    // 将字节数组填充到 Uint8Array 中
    for (let i = 0; i < byteCharacters.length; i++) {
        byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    // 创建 Blob 对象
    const blob = new Blob([byteArray], { type: mimeType });
    // 将 Blob 对象的 URL 赋值给 a 标签的 href 属性
    a.href = URL.createObjectURL(blob);
    // 将a标签暂时添加到 body 中，触发下载
    document.body.appendChild(a);
    a.click();
    // 下载完成后，将 a 标签从 body 中移除
    document.body.removeChild(a);
}