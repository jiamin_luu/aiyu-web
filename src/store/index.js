import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        selectionKnowledge: [{
            id: 1,
            label: "公共文档库"
        }],
        overallLoading: false
    },
    getters: {},
    mutations: {
        toggle_selectionKnowledge: (state, data) => {
            state.selectionKnowledge = data
        },
        toggle_overallLoading: (state, bool) => {
            state.overallLoading = bool
        },
    },
    actions: {},
    modules: {}
})