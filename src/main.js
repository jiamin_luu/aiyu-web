import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import "@/assets/scss/base.scss";
import '@/assets/font/iconfont.css'
import '@/assets/scss/costomStyle.scss'

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI)

import myEchart from '@/components/myEcharts/index.vue' // echarts
Vue.component('myEchart', myEchart)

const BUS = new Vue()
Vue.prototype.$Bus = BUS

import dayjs from 'dayjs'
Vue.prototype.$dayjs = dayjs

Vue.config.productionTip = false
new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')