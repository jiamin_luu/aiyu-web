import base from '@/utils/http/base' // 导入接口域名列表
import http from '@/utils/http/index' // 导入http中创建的axios实例

// 助手问答
// export const knowledgeBaseChat = (params) => {
//     return http.post(base.url + '/chat/knowledge_base_chat', params)
// }


// 问答
export const QAmain = (params) => {
    return http.post(base.lurl + '/qa/main', params);
}


// 文档问答
export const knowledgeChatMultibase = (params) => {
    return http.post(base.lurl + '/qa/multibase_chat', params);
    // return http.post(base.url + '/chat/knowledge_base_chat_multibase', params)
}




// 获取用户问题记录
export const listSession = (params) => {
    return http.post(base.lurl + '/qa/list_session', params)
}

// 删除根据id删除某条记录
export const deleteForSession = (params) => {
    return http.post(base.lurl + '/qa/logic_delete_session', params)
}

// 根据sessionId获取记录详情
export const detailsForSession = (params) => {
    return http.post(base.lurl + '/qa/session_details', params)
        // return http.post(base.url + '/chat/knowledge_base_chat_multibase', params)
}

// 回答结果评分
export const markZhishui = (params) => {
    return http.post(base.lurl + '/qa/mark_zhishui', params)
        // return http.post(base.url + '/chat/knowledge_base_chat_multibase', params)
}