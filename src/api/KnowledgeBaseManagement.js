import base from '@/utils/http/base' // 导入接口域名列表
import http from '@/utils/http/index' // 导入http中创建的axios实例

// 登录
export const login = (params) => {
    return http.post(base.lurl + '/login', params)
}

// 退出登录
export const outLogin = (params) => {
    return http.post(base.lurl + '/logout', params)
}



// 获取知识库列表
export const knowledgeListBases = (params) => {
    return http.post(base.lurl + '/file-library/list', params);
    // return http.get(base.url + '/knowledge_base/list_knowledge_bases', { params })
}

// 创建知识库
export const createKnowledge = (params) => {
    return http.post(base.lurl + '/knowledge_base/add', params);
    // return http.post(base.lurl + '/file-library/add', params)
};

// 创建知识库(模型)
export const createKnowledgeModel = (params) => {
    return http.post(base.url + '/knowledge_base/create_knowledge_base', params)
}

// 删除知识库
export const deleteKnowledge = (params) => {
    return http.post(base.lurl + '/knowledge_base/delete', params)
        // return http.post(base.lurl + '/file-library/logic-delete', params)
}

// 删除知识库(模型)
export const deleteKnowledgeModel = (params) => {
    return http.post(base.url + '/knowledge_base/delete_knowledge_base', params)
}


// 修改知识库(模型)
export const updateInfo = (params) => {
    return http.post(base.url + '/knowledge_base/update_info', params)
}

// 修改知识库(Java)
export const updateInfoJava = (params) => {
    return http.post(base.lurl + '/file-library/update', params)
}







// 获取知识库内的文件列表_附下载链接
export const listFiles = (params) => {
    return http.get(base.url + '/knowledge_base/list_files_with_details', { params })
}
export const listFilesJava = (params) => {
    return http.post(base.lurl + '/file/list', params)
}

// 上传文件
export const uploadDocs = (params) => {
    return http.post(base.url + '/knowledge_base/upload_docs', params)
};
// 上传文件(java)
export const uploadDocsJava = (params) => {
    return http.post(base.lurl + '/file/upload', params);
    // return http.post(base.lurl + '/file/add', params)
}


// 删除文件
export const deleteDocs = (params) => {
    return http.post(base.url + '/knowledge_base/delete_docs', params)
};
// 删除文件（java）
export const deleteDocsJava = (params) => {
    return http.post(base.lurl + '/file/delete', params)
}


// 修改文件
// export const updateDocsJa = (params) => {
//     return http.post(base.lurl + '/file/update', params)
// }
// 修改文件（java）
export const updateDocsJava = (params) => {
    return http.post(base.lurl + '/file/update', params)
}

// 批量问答
export const createTempKnowledge = (params) => {
    return http.post(base.lurl + '/knowledge_base/temp_add', params)
        // return http.post(base.url + '/knowledge_base/create_temp_knowledge_base', params)
}




// 获取txt文件内容
export const getTxtFile = (params) => {
    return http.get(base.url + params.url, { responseType: "text" })
}