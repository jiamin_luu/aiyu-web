import base from '@/utils/http/base' // 导入接口域名列表
import http from '@/utils/http/index' // 导入http中创建的axios实例

// 工具集查询
export const toolkitList = (params) => {
    return http.post(base.lurl + '/toolkit/list', params)
};
// 工具集修改
export const toolkitUpdate = (params) => {
    return http.post(base.lurl + '/toolkit/update', params)
};
// 工具集添加
export const toolkitAdd = (params) => {
    return http.post(base.lurl + '/toolkit/add', params)
};
// 工具集上传文件
export const toolkitRegistry = (params) => {
    return http.post(base.lurl + '/toolkit/registry', params)
};
// 工具集删除
export const toolkitDelete = (params) => {
    return http.post(base.lurl + '/toolkit/logic-delete', params);
    // return http.post(base.lurl + '/toolkit/delete', params)
}


// 注册工具查询
export const registryList = (params) => {
    return http.post(base.lurl + '/registry/list', params)
};
// 注册工具删除
export const registryDelete = (params) => {
    return http.post(base.lurl + '/registry/logic-delete', params)
};
// 注册工具上传
export const registryAdd = (params) => {
    return http.post(base.lurl + '/registry/register', params)
        // return http.post(base.lurl + '/registry/add', params)
};
// 检测链接是否正常
export const registryCheck = (params) => {
    return http.post(base.lurl + '/registry/check-service?id=' + params.id)
}



// 工具prompt新增
export const toolPromptAdd = (params) => {
    return http.post(base.lurl + '/tool-prompt/add', params)
};
// 工具集prompt修改
export const tkPromptUpdate = (params) => {
    return http.post(base.lurl + '/toolkit/update-prompt', params);
    // return http.post(base.lurl + '/tool-prompt/update', params)
};
// 工具prompt修改
export const toolPromptUpdate = (params) => {
    return http.post(base.lurl + '/registry/update', params);
    // return http.post(base.lurl + '/tool-prompt/update', params)
};
// 工具prompt删除
export const toolPromptDelete = (params) => {
    return http.post(base.lurl + '/tool-prompt/delete', params)
};
// 工具集prompt查询
export const tkPromptList = (params) => {
        return http.post(base.lurl + '/toolkit/searchById', params)
    }
    // 工具prompt查询
export const toolPromptList = (params) => {
    return http.post(base.lurl + '/registry/getInfo/' + params);
    // return http.post(base.lurl + '/tool-prompt/list', params)
}


// 工具shot添加
export const shotAdd = (params) => {
    return http.post(base.lurl + '/shot_library/add', params)
};
// 工具shot查询
export const shotList = (params) => {
    return http.post(base.lurl + '/shot_library/list', params)
};
// 工具shot修改
export const shotUpdate = (params) => {
    return http.post(base.lurl + '/shot_library/update', params)
};
// 工具shot删除
export const shotDelete = (params) => {
    return http.post(base.lurl + '/shot_library/delete', params)
}